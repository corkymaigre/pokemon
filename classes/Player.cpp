#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsScene>

#include "classes/Player.hpp"
#include "classes/Sprite.hpp"

Player::Player(QGraphicsScene *scene) : QGraphicsPixmapItem()
{
   this->scene  = scene;
   this->sprite = new Sprite(QPixmap::fromImage(QImage(":/images/images/ash.png")), 35, 50);
   setPixmap(this->sprite->getSprite(this->sprite->getDownSprites(), this->sprite->getDownSpritesIndex()));
}

void Player::keyPressEvent(QKeyEvent *event)
{
   switch (event->key())
   {
   case Qt::Key_Left:
      setPixmap(this->sprite->getSprite(this->sprite->getLeftSprites(), this->sprite->getLeftSpritesIndex()));
      this->scene->setSceneRect(this->scene->sceneRect().x() - 10,
                                this->scene->sceneRect().y(),
                                this->scene->width(),
                                this->scene->height()
                                );
      setPos(x() - 10, y());
      break;

   case Qt::Key_Right:
      setPixmap(this->sprite->getSprite(this->sprite->getRightSprites(), this->sprite->getRightSpritesIndex()));
      this->scene->setSceneRect(this->scene->sceneRect().x() + 10,
                                this->scene->sceneRect().y(),
                                this->scene->width(),
                                this->scene->height()
                                );
      setPos(x() + 10, y());
      break;

   case Qt::Key_Up:
      setPixmap(this->sprite->getSprite(this->sprite->getUpSprites(), this->sprite->getUpSpritesIndex()));
      this->scene->setSceneRect(this->scene->sceneRect().x(),
                                this->scene->sceneRect().y() - 10,
                                this->scene->width(),
                                this->scene->height()
                                );
      setPos(x(), y() - 10);
      break;

   case Qt::Key_Down:
      setPixmap(this->sprite->getSprite(this->sprite->getDownSprites(), this->sprite->getDownSpritesIndex()));
      this->scene->setSceneRect(this->scene->sceneRect().x(),
                                this->scene->sceneRect().y() + 10,
                                this->scene->width(),
                                this->scene->height()
                                );
      setPos(x(), y() + 10);
      break;

   default: break;
   }
}
