#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QMediaPlayer>
#include <QGraphicsScene>

#include "classes/Sprite.hpp"

class Player : public QObject, public QGraphicsPixmapItem {
   Q_OBJECT
public:
   Player(QGraphicsScene *scene);
   void keyPressEvent(QKeyEvent *event);

private:
   QPixmap pixmap;
   QGraphicsScene *scene;
   Sprite *sprite;
};


#endif // PLAYER_HPP
