#include <QTimer>
#include <QGraphicsTextItem>
#include <QFont>
#include <QMediaPlayer>
#include <QBrush>
#include <QImage>
#include <QPixmap>

#include "classes/Game.hpp"

#include <QDebug>

Game::Game(QWidget *parent)
{
   scene = new QGraphicsScene();

   QPixmap pixmap = QPixmap::fromImage(QImage(":/images/images/map.png"));
   scene->setSceneRect(2700, 11485, 385, 50);
   scene->addPixmap(pixmap.scaled(17500, 17500, Qt::KeepAspectRatio, Qt::SmoothTransformation));


   // make the newly created scene the scene to visualize (since Game is a QGraphicsView Widget,
   // it can be used to visualize scenes)
   setScene(scene);
   setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   setFixedSize(1366, 768);

   // create the player
   player = new Player(scene);
   player->setPos(2800, 11600); // TODO generalize to always be in the middle bottom of screen
   // make the player focusable and set it to be the current focus
   player->setFlag(QGraphicsItem::ItemIsFocusable);
   player->setFocus();
   // add the player to the scene
   scene->addItem(player);


   // play background music
   QMediaPlayer *music = new QMediaPlayer();
   music->setMedia(QUrl("qrc:/sounds/background-sound.mp3"));
   music->play();

   show();
}
