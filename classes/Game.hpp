#ifndef GAME_HPP
#define GAME_HPP

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include "Player.hpp"

class Game : public QGraphicsView {
public:
   Game(QWidget *parent = 0);

   QGraphicsScene *scene;
   Player *player;
};

#endif // GAME_HPP
