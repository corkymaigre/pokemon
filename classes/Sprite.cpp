#include <QKeyEvent>

#include "Sprite.hpp"

Sprite::Sprite(QPixmap pixmap, int width, int height) : QGraphicsPixmapItem()
{
   this->down_sprites        = new QPixmap [4];
   down_sprites[0]           = pixmap.copy(15, 10, width, height);
   down_sprites[1]           = pixmap.copy(75, 10, width, height);
   down_sprites[2]           = pixmap.copy(140, 10, width, height);
   down_sprites[3]           = pixmap.copy(205, 10, width, height);
   this->up_sprites_index    = 0;
   this->left_sprites        = new QPixmap [4];
   left_sprites[0]           = pixmap.copy(15, 75, width, height);
   left_sprites[1]           = pixmap.copy(75, 75, width, height);
   left_sprites[2]           = pixmap.copy(140, 75, width, height);
   left_sprites[3]           = pixmap.copy(205, 75, width, height);
   this->left_sprites_index  = 0;
   this->right_sprites       = new QPixmap [4];
   right_sprites[0]          = pixmap.copy(15, 140, width, height);
   right_sprites[1]          = pixmap.copy(75, 140, width, height);
   right_sprites[2]          = pixmap.copy(140, 140, width, height);
   right_sprites[3]          = pixmap.copy(205, 140, width, height);
   this->right_sprites_index = 0;
   this->up_sprites          = new QPixmap [4];
   up_sprites[0]             = pixmap.copy(15, 205, width, height);
   up_sprites[1]             = pixmap.copy(75, 205, width, height);
   up_sprites[2]             = pixmap.copy(140, 205, width, height);
   up_sprites[3]             = pixmap.copy(205, 205, width, height);
   this->down_sprites_index  = 0;
}

QPixmap Sprite::getSprite(QPixmap *sprites, int&index)
{
   QPixmap sprite = sprites[index];

   this->updateSpriteIndex(index);
   return(sprite);
}

void Sprite::updateSpriteIndex(int&index)
{
   index++;
   if (index >= 4)
   {
      index -= 4;
   }
}

QPixmap *Sprite::getLeftSprites()
{
   return(this->left_sprites);
}

int& Sprite::getLeftSpritesIndex()
{
   return(this->left_sprites_index);
}

void Sprite::setLeftSpritesIndex(int index)
{
   this->left_sprites_index = index;
}

QPixmap *Sprite::getRightSprites()
{
   return(this->right_sprites);
}

int& Sprite::getRightSpritesIndex()
{
   return(this->right_sprites_index);
}

void Sprite::setRightSpritesIndex(int index)
{
   this->right_sprites_index = index;
}

QPixmap *Sprite::getUpSprites()
{
   return(this->up_sprites);
}

int& Sprite::getUpSpritesIndex()
{
   return(this->up_sprites_index);
}

void Sprite::setUpSpritesIndex(int index)
{
   this->up_sprites_index = index;
}

QPixmap *Sprite::getDownSprites()
{
   return(this->down_sprites);
}

int& Sprite::getDownSpritesIndex()
{
   return(this->down_sprites_index);
}

void Sprite::setDownSpritesIndex(int index)
{
   this->down_sprites_index = index;
}
