#ifndef SPRITE_HPP
#define SPRITE_HPP

#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QString>

class Sprite : public QGraphicsPixmapItem {
public:
   Sprite(QPixmap pixmap, int width, int height);
   QPixmap getSprite(QPixmap *sprites, int&index);
   void updateSpriteIndex(int&index);
   QPixmap *getLeftSprites();
   int& getLeftSpritesIndex();
   void setLeftSpritesIndex(int index);
   QPixmap *getRightSprites();
   int& getRightSpritesIndex();
   void setRightSpritesIndex(int index);
   QPixmap *getUpSprites();
   int& getUpSpritesIndex();
   void setUpSpritesIndex(int index);
   QPixmap *getDownSprites();
   int&getDownSpritesIndex();
   void setDownSpritesIndex(int index);

private:
   int width;
   int height;
   int left_sprites_index;
   int right_sprites_index;
   int up_sprites_index;
   int down_sprites_index;
   QPixmap *left_sprites;
   QPixmap *right_sprites;
   QPixmap *up_sprites;
   QPixmap *down_sprites;
};

#endif // SPRITE_HPP
